/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.injections;

import FBMY.Config;
import FBMY.requesters.Model;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MYRV
 */
public class ModelsBuilder {
    public static String modelsDir;
    
    public static void UseDefault() {
        modelsDir = Config.APPDIR + "src/App/Models";
    }
    
    public static void run() {
        var baseDirectory = modelsDir.replace(".", "/");
        var packages = ListModels(baseDirectory, getFolder());
        var classes = pckgsToClasses(packages);
        for (var modelClass : classes) {
            injectModel(modelClass);
        }
    }
    
    public static List<String> ListModels(String directory, File folder) {
        List<String> response = new ArrayList<>();
        for (final var entry : folder.listFiles()) {
            var entryDir = directory + "/" + entry.getName();
            if (entry.isDirectory()) {
                var items = ListModels(entryDir, entry);
                for (var item : items) response.add(item);
                continue;
            }
            response.add(dirToPackage(entryDir));
        }
        return response;
    }
    
    private static File getFolder() {
        return new File(modelsDir);
    }
    
    private static String getDataFileName(Class modelClass) {
        var fragments = modelClass.getName().replace(".", "/").split("/");
        var name = fragments[fragments.length-1];
        return name.toLowerCase() + "s.csv";        
    }
    
    private static String dirToPackage(String directory) {
        directory = directory.replace("/", ".").replace("\\", ".");
        var sources = directory.split("src.");
        var appSource = sources[sources.length-1];
        return appSource.replace(".java", "");
    }
    
    private static List<Class> pckgsToClasses(List<String> pckgs) {
        List<Class> Classes = new ArrayList<>();
        for (String pckg : pckgs) {
            try {
                Classes.add(Class.forName(pckg));
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ModelsBuilder.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return Classes;
    }
    
    static void injectModel(Class modelClass) {
        try {
            var field = modelClass.getField("ctx");
            field.setAccessible(true);
            
            changeModifiersOf(field);
            var dataFileName = Config.DataDir() + getDataFileName(modelClass);
            field.set(null, new Model(modelClass, dataFileName));
            
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException ex) {
            Logger.getLogger(ModelsBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static void changeModifiersOf(Field field) {
        try {
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException ex) { }
    }
    
}
