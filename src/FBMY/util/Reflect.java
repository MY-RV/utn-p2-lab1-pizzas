/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.util;

import FBMY.navigation.Kangaroo;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MYRV
 */
public class Reflect {
    static public Method getMethod(Object obj, String name) {
        try {
            return obj.getClass().getMethod(name);
        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(Reflect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    static public Object runMethod(Object obj, String name) {
        try {
            return getMethod(obj, name).invoke(obj);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(Reflect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static Object[] buildRawParams(Parameter[] parameters) {
        List<Object> response = new ArrayList<>();
        try {
            for (var param : parameters) {
                var contructor = param.getType().getConstructors()[0];
                response.add(contructor.newInstance(new Object[0]));
            }
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(Kangaroo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response.toArray(Object[]::new);
    }
    
    public static <T>T buildRawInstance(Class<T> instanceClass) {
        try {
            var constructor = instanceClass.getConstructors()[0];
            var parameters = buildRawParams(constructor.getParameters());
            return (T) constructor.newInstance(parameters);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(Kangaroo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
