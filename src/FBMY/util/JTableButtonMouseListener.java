/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.util;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTable;

/**
 *
 * @author MYRV
 */
public class JTableButtonMouseListener extends MouseAdapter {
    public interface CallBack { void send(int row, int col); }
    private final JTable table;
    private final CallBack CBack;

    public JTableButtonMouseListener(JTable table, CallBack call) {
        this.table = table;
        this.CBack = call;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        CBack.send(e.getY()/table.getRowHeight(), e.getX()/table.getRowHeight());
    }

}
