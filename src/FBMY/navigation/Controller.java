/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.navigation;

/**
 *
 * @author MYRV
 * @param <F>
 */
public class Controller<F extends Frame> {
    public final F frame;
    
    public Controller(F frame) { 
        this.frame = frame; 
    }
    
    public void index() {}
    public F getFrame() {
        return this.frame;
    }
}
