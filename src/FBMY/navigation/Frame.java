/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.navigation;

import FBMY.types.IdName;
import javax.swing.*;

/**
 *
 * @author MYRV
 * @param <C>
 */
public class Frame<C extends Controller> extends JFrame {
    public C controller;
    public boolean loading = false;
    public Frame(Class<C> ...C) { super(); }
    
    public void setUp() {}
    
    @Override
    public void setSize(int WIDTH, int HEIGHT) {
        WIDTH+= 14;
        HEIGHT+= 38;
        super.setSize(WIDTH, HEIGHT);
    }
    
    public <T>IdName<Integer,T> getIdName(JComboBox<T> comboBox) {
        int index = comboBox.getSelectedIndex();
        if (index == -1) return null;
        return new IdName(index, (T)comboBox.getSelectedItem());
    }
    
    public IdName<Integer,String> getIdName(ButtonGroup group) {
        int index = 0;
        for (var buttons = group.getElements(); buttons.hasMoreElements();) {
            var button = buttons.nextElement();
            if (button.isSelected()) return new IdName(index, button.getText());
            index++;
        } return null;
    }
    
    public <T>T getValue(JComboBox<T> comboBox) {
        return (T) comboBox.getSelectedItem();
    }
    
    public String getValue(ButtonGroup group) {
        for (var buttons = group.getElements(); buttons.hasMoreElements();) {
            var button = buttons.nextElement();
            if (button.isSelected()) return button.getText();
        } return null;
    }
    
    public String getValue(JTextField field) {
        return  field.getText();
    }
    
}
