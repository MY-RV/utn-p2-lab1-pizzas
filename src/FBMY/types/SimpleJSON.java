/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.types;

import java.util.*;
import java.util.regex.Pattern;

/**
 *
 * @author MYRV
 */
public class SimpleJSON extends ArrayList<Map<String, String>>{
    private char quote = '"';
    
    public SimpleJSON() { super(); }
    public SimpleJSON(String value) { super(); map(quote, value); }
    public SimpleJSON(char divider) { super(); this.quote = divider; }
    public SimpleJSON(char divider, String value) { super(); map(divider, value); }
    
    private void map(char divider, String value) {
        new SimpleJSONCompiler(divider).mapInto(value, this);
        this.quote = divider;
    }

    public Map<String, String> newRow() {
        return this.push(new HashMap<>());
    }

    public Map<String, String> newRow(String key, String value) {
        var newItem = this.push(new HashMap());
        newItem.put(key, value);
        return newItem;
    }

    public Map<String, String> push(Map<String, String> newItem) {
        super.add(newItem);
        return newItem;
    }
    
    public List<String> pluck(String key) {
        List<String> response = new ArrayList<>();
        for (var row : this) {
            var value = row.get(key);
            if (value != null) response.add(value);
        }
        return response;
    }
    
    public List<String> pluckNUllS(String key) {
        List<String> response = new ArrayList<>();
        for (var row : this) 
            response.add(row.get(key));
        return response;
    }
    
    @Override
    public String toString() {
        var response = "";
        for (var row : this) {
            var object = "";
            for (var entry : row.entrySet()) {
                if (!object.isEmpty()) object += ", ";
                object += quoteStr(entry.getKey());
                object += ":";
                object += quoteStr(entry.getValue());
            }
            if (!response.isEmpty()) response += ", ";
            response += "{" + object + "}";
        }
        return "[" + response + "]";
    }
    
    private String quoteStr(String str) {
        if (str.matches("(\\d+|false|true|^([+-]?\\d*\\.?\\d*)$)"))
            return str;
        return quote + str + quote;
    }

    private static class SimpleJSONCompiler {
        String regex = "\"([^\"]+)\":[\s,\"]*([^,^\\}^\"]+)";

        SimpleJSONCompiler() { super(); }
        SimpleJSONCompiler(char quote) { 
            super(); regex = regex.replace('"', quote);
        }

        SimpleJSON mapInto(String string, SimpleJSON jsonList) {
            string = string.replace("}, {", "},").replace("},{", "},");
            String objects[] = string.split("},");
            Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            for (String object : objects) {
                var matcher = pattern.matcher(object);
                var json = jsonList.newRow();
                while (matcher.find()) {
                    json.put(matcher.group(1), matcher.group(2));
                }    
            }
            return jsonList;
        }

    }
    
}
