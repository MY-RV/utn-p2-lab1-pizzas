/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.treats.fileDealers.dataDealers;

import FBMY.util.Closures.IDataRowTrait;
import FBMY.treats.dataWorkers.Racoon;
import FBMY.treats.fileDealers.extension.Parser;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author MYRV
 * @param <T>
 */
public class DataCSV<T> {
        private final IDataRowTrait<T> trait;
        private final Racoon<T> racoon;
        private int routine = 0; // Number of row Insertion Attep
        public String[] headers;
        public List<T> rows = new ArrayList<>();

        public static <T>DataCSV<T> For(
            Racoon<T> maker, IDataRowTrait trait
        ) { return new DataCSV(maker, trait); }
        private DataCSV(Racoon<T> maker, IDataRowTrait recordTrait) { 
            this.trait = recordTrait; 
            this.racoon = maker; 
        }

        public void addRow(String str) { addRow(Parser.csvLine.from(str)); }
        public void addRow(String[] args) {
            this.routine++;
            if (args.length != headers.length) {
                for (int i = 0; i < headers.length; i++) {
                    System.out.println(headers[i]);
                    System.out.println(args[i]);
                }
                System.out.println(
                    "(!) FAIL.DataCSV | Invalid row in rutine: " + routine + 
                    "\r\n    " +
                    "Expected " + headers.length + " Found " + args.length +
                    " (headers/columns) \r\n"
                ); return;
            }
            var newRow = racoon.create((r) -> {
                for (int i = 0; i < headers.length; i++) {
                    r.set(headers[i], args[i]);
                }
            });
            if (trait != null) newRow = trait.run(newRow);
            if (newRow != null) this.rows.add(newRow);
        }
}
