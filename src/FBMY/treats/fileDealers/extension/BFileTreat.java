/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.treats.fileDealers.extension;

/**
 *
 * @author MYRV
 * @param <Reader>
 * @param <Data>
 */
public abstract class BFileTreat<Reader, Data> implements IReader<Data> {
    protected IReader.PathPrefix PathPrefix;
    public final String FilePath;
    
    public BFileTreat(String FilePath) { 
        this.FilePath = FilePath; 
    }
    
    @Override
    public String FilePath() {
        if (PathPrefix == null) return FilePath;
        return PathPrefix.get() + FilePath;
    }
    
    @Override
    public Reader withPrefix(IReader.PathPrefix prefixer) {
        this.PathPrefix = prefixer;
        return (Reader) this;
    }
}
