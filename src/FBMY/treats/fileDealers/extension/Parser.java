package FBMY.treats.fileDealers.extension;


import java.util.ArrayList;
import java.util.regex.Pattern;

public class Parser {
    public static class csvLine {
        private static final csvLine parser = new csvLine();
        private final ArrayList<String> matches = new ArrayList<>();
        private final Pattern csvPattern = Pattern.compile("\"([^\"]*)\"|(?<=,|^)([^,]*)(?:,|$)");

        public static String[] from(String csvLine){
            return parser.getFrom(csvLine);
        }

        private String[] getFrom(String csvLine) {
            var matcher = csvPattern.matcher(csvLine);
            matches.clear();
            String match;
            while (matcher.find()) {
                match = matcher.group(1);
                matches.add(((match!=null) ? match : matcher.group(2)).strip());
            }

            if (matches.isEmpty()) return new String[0];
            return matches.toArray(String[]::new);
        }
    }
}