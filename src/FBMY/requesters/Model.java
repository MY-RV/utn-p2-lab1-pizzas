/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.requesters;

import FBMY.treats.dataWorkers.Racoon;
import FBMY.treats.fileDealers.OwlCSV;
import FBMY.util.Closures.*;
import java.util.Date;

/**
 *
 * @author MYRV
 * @param <T>
 */
public class Model<T> extends Context<T> {
    
    public Model(Class<T> ModelClass, String FilePath) { super(ModelClass, FilePath); }
    
    public T create(IRunnable<Racoon.Cooper> builder) {
        var racoon = Racoon.For(ContextClass);
        var result = racoon.create(builder);
        OwlCSV.For(racoon, FilePath).addData(result);
        return result;
    }
    
    public Context<T> query() { return Context(); }
    private Context<T> Context() { return new Context(ContextClass, FilePath); }

    @Override
    public Context<T> where(String key, String value) { return Context().where(key, value); };
    @Override
    public Context<T> where(String key, String operator, Integer value) { return Context().where(key, operator, value); }
    @Override
    public Context<T> where(String key, String operator, Float value) { return Context().where(key, operator, value); }
    @Override
    public Context<T> where(String key, String operator, Date value) { return Context().where(key, operator, value); }
    @Override
    public Context<T> where(ICondition<T> newCondition) { return Context().where(newCondition); };
    @Override
    public <V> Context<T> where(String key, V value) { return Context().where(key, value); }
    @Override
    public Context<T> when(boolean condition, IRunnable<Context<T>> closure) { return Context().when(condition, closure); }    
    @Override
    public Context<T> when(String string, IRunnable<Context<T>> closure) { return Context().when(string, closure); }
    @Override
    public Context<T> when(Object value, IRunnable<Context<T>> closure) { return Context().when(value, closure); }
}
