/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.requesters;

import FBMY.treats.dataWorkers.Racoon;
import FBMY.treats.fileDealers.OwlCSV;
import FBMY.util.Closures.*;
import FBMY.util.Reflect;
import java.util.*;

/**
 *
 * @author MYRV
 * @param <T>
 */
public class Context<T> extends OperatorsHelper<T> {
    public final String FilePath;
    public final Class<T> ContextClass; 
    public List<ICondition<T>> Conditions = new ArrayList<>();
    
    
    public Context(Class<T> ContextClass, String FilePath) {
        super();
        this.ContextClass = ContextClass;
        this.FilePath = FilePath;
    }  
    
    public Context<T> where(String key, String value) {
        Conditions.add((arg) -> {
            var argValue = (String) Reflect.runMethod(arg, key);
            return argValue.toLowerCase().equals(value.toLowerCase());
        });
        return this;
    }
    
    public Context<T> where(String key, String operator, Integer value) {
        var condition = IntegerOperators.get(operator);
        if (condition == null) return this;
        Conditions.add((obj) -> {
            var arg = (Integer) Reflect.runMethod(obj, key);
            if (arg == null || obj == null) return false;
            return condition.run(arg, value);
        });
        return this;
    }
    
    public Context<T> where(String key, String operator, Float value) {
        var condition = FloatOperators.get(operator);
        if (condition == null) return this;
        Conditions.add((obj) -> {
            var arg = (Float) Reflect.runMethod(obj, key);
            if (arg == null || obj == null) return false;
            return condition.run(arg, value);
        });
        return this;
    }
    
    public Context<T> where(String key, String operator, Date value) {
        var condition = DateOperators.get(operator);
        if (condition == null) return this;
        Conditions.add((obj) -> {
            var arg = (Date) Reflect.runMethod(obj, key);
            if (arg == null || obj == null) return false;
            return condition.run(arg, value);
        });
        return this;
    }
    
    public <V> Context<T> where(String key, V value) {
        Conditions.add((arg) -> {
            var argValue = (V) Reflect.runMethod(arg, key);
            return Objects.equals(argValue, value);
        });
        return this;
    }
    
    public Context<T> where(ICondition<T> newCondition) {
        Conditions.add(newCondition);
        return this;
    }
    
    public Context<T> when(boolean condition, IRunnable<Context<T>> closure) {
        if (condition) closure.run(this);
        return this;
    }
    
    public Context<T> when(String string, IRunnable<Context<T>> closure) {
        if (!string.isEmpty()) closure.run(this);
        return this;
    }
    
    public Context<T> when(Object value, IRunnable<Context<T>> closure) {
        if (value != null) closure.run(this);
        return this;
    }
    
    public List<T> get() {
        var owlRead = OwlCSV.For(Racoon.For(ContextClass), FilePath);
        if (Conditions.isEmpty()) return owlRead.getData().rows;
        return owlRead.getData((T row) -> {
            Boolean preserve = null;
            for (ICondition<T> Condition : Conditions) {
                if (preserve == null) preserve = Condition.run(row);
                else preserve = preserve && Condition.run(row);
            }
            return preserve ? row : null;
        }).rows;
    }
    
    public void update(T Record) {
        var racoon = Racoon.For(ContextClass);
        var owlDealer = OwlCSV.For(racoon, FilePath);
        if (Conditions.isEmpty()) return;
        owlDealer.setData(owlDealer.getData((T row) -> {
            Boolean update = null;
            for (ICondition<T> Condition : Conditions) {
                if (update == null) update = Condition.run(row);
                else update = update && Condition.run(row);
            }
            
            if (!update) return row;
            return racoon.merge(row, Record);
        }));
    }    
    
    public T first() { return get().get(0); }    
    
}
