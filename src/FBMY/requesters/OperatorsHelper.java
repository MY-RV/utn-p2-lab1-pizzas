/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.requesters;

import FBMY.util.Closures.IComparation;
import java.util.*;

/**
 *
 * @author MYRV
 * @param <T>
 */
public class OperatorsHelper<T> {
    Map<String, IComparation<Integer>> IntegerOperators = new HashMap<>(){{
        put(">",  (arg, value) -> (arg > value));
        put(">=", (arg, value) -> (arg >= value));
        put("<=", (arg, value) -> (arg <= value));
        put("<",  (arg, value) -> (arg < value));
        put("!=", (arg, value) -> (!Objects.equals(arg, value)));
    }};
    
    Map<String, IComparation<Float>> FloatOperators = new HashMap<>(){{
        put(">",  (arg, value) -> (arg > value));
        put(">=", (arg, value) -> (arg >= value));
        put("<=", (arg, value) -> (arg <= value));
        put("<",  (arg, value) -> (arg < value));
        put("!=", (arg, value) -> (!Objects.equals(arg, value)));
    }};
    
    Map<String, IComparation<Date>> DateOperators = new HashMap<>(){{
        put(">",  (arg, value) -> (arg.after(value)));
        put(">=", (arg, value) -> (arg.after(value) || arg.equals(value)));
        put("<=", (arg, value) -> (arg.before(value) || arg.equals(value)));
        put("<",  (arg, value) -> (arg.before(value)));
        put("!=", (arg, value) -> (!Objects.equals(arg, value)));
    }};
    
}
