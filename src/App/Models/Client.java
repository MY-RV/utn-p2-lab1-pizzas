/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Record.java to edit this template
 */
package App.Models;

import FBMY.requesters.Model;
import java.util.UUID;

/**
 *
 * @author Yor
 */
public record Client(
    UUID    id,
    String  identification,
    String  name,
    String  gender,
    String  province,
    Role    role
) { public static Model<Client> ctx; 
    public enum Role { user, admin; }
}
