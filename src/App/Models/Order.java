/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Record.java to edit this template
 */
package App.Models;

import App.Lib.Pizza;
import FBMY.requesters.Model;
import FBMY.types.DateTime;
import FBMY.types.SimpleJSON;
import java.util.ArrayList;
import java.util.UUID;

/**
 *
 * @author Yor
 */
public record Order(
    UUID        id,
    UUID        client_id,
    String      province,
    SimpleJSON  pizzas,
    AddonsList  addons,
    String      discount_code,
    Float       raw_price,
    Float       final_price,
    DateTime    created_at
) { public static Model<Order> ctx;
    public static enum Addon {
        Soda(1500), Dessert(1500), Garlic_Bread(1000);

        public Integer price;
        private Addon(int value) {
            price = value;
        }
    }

    public static class AddonsList extends ArrayList<Addon> {

        @Override
        public String toString() {
            var result = "";
            for (Addon addon : this) {
                if (addon == null) continue;
                if (!result.equals("")) result += ",";
                result += addon.toString();
            } return result;
        }

    }
}
