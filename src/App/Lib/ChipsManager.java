/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Lib;

import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Yor
 */
public class ChipsManager extends HashMap<String, Boolean> {
    public ChipsManager(List chips, Boolean defValue) {
        chips.forEach(chip -> {
            var name = chip.toString();
            put(name.substring(0, 1).toUpperCase() + name.substring(1), defValue);
        });
    }
    public ChipsManager(Object[] chips, Boolean defValue) {
        this(List.of(chips), defValue);
    }

    @Override
    public String toString() {
        var response = "";
        for (var entry : this.entrySet()) {
            if (!entry.getValue()) continue;
            if (!response.isBlank()) response += ", ";
            response += entry.getKey();
        } return response;
    }
    
    public void reset() {
        for (var entry : entrySet()) put(entry.getKey(), true);
    }
    
}
