/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Lib;

import java.util.List;

/**
 *
 * @author Yor
 */
public class Pizza {
    public static enum Dough { 
        Thin(.1f), Thick(.05f); 
        
        public final Float percent;
        private Dough(float value) { percent = value; }
    }
    public static enum Size { 
        Median(5500), Big(6500), Familiar(7500); 
        
        public final Integer price;
        private Size(int value) { price = value; }
    }
    public static enum Type {
        Pepperoni("queso", "pepperoni"),
        Hawaiana("queso", "jamón", "piña"),
        Suprema("queso", "jamón", "pepperoni", "cebolla", "chile dulce", "aceitunas"),
        Napolitana("tomate", "ajo", "mozzarella", "albahaca");
        
        public final List<String> ingredients;
        private Type(String ...items) { ingredients = List.of(items); }
    }
    
}
