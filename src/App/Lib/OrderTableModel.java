/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Lib;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Yor
 */
public final class OrderTableModel extends DefaultTableModel {
    public static enum Columns { IN, Type, Ingredients, Size, Dough, Price };
    final Color DISABLED_COLOR = new Color(200,200,200);
    List<Boolean> rowEnables;
    List<Color> rowColours;
    Class[] types;
    
    public OrderTableModel() {
        super(new Object[][]{}, Columns.values());
        types = new Class[]{
            Boolean.class, Pizza.Type.class, ChipsManager.class, 
            Pizza.Size.class, Pizza.Dough.class, Float.class};
        rowColours = new ArrayList<>();
        rowEnables = new ArrayList<>();
        setRowCount(0);
        for (var value : Pizza.Type.values()) addRow(new Object[]{
            false, value, new ChipsManager(value.ingredients, true),
            Pizza.Size.Median, Pizza.Dough.Thin,
            Pizza.Size.Median.price + (
                Pizza.Size.Median.price * Pizza.Dough.Thin.percent
            ),
        });
    }
    
    public void recalculate(int row) {
        var isChecked = (Boolean) getValueAt(row, Columns.IN.ordinal());
        if (!isChecked) {
            setValueAt(Pizza.Size.Median.price + (
                Pizza.Size.Median.price * Pizza.Dough.Thin.percent
            ), row, Columns.Price.ordinal());
        } else {
            var size = (Pizza.Size) getValueAt(row, Columns.Size.ordinal());
            var dough = (Pizza.Dough) getValueAt(row, Columns.Dough.ordinal());
            setValueAt(size.price + (size.price * dough.percent), row, Columns.Price.ordinal());
        }
        
    }

    @Override
    public void addRow(Object[] rowData) {
        rowColours.add(DISABLED_COLOR);
        rowEnables.add(false);
        super.addRow(rowData);
    }
    
    
    @Override public Class getColumnClass(int columnIndex) {
        return types[columnIndex];
    }
    @Override public boolean isCellEditable(int row, int col) {
        return col == Columns.IN.ordinal() || (
            rowEnables.get(row) && (
                col == Columns.Size.ordinal() || 
                col == Columns.Dough.ordinal()
            )
        );
    }

    public void reloadRow(int row) {
        var enable = (Boolean) getValueAt(row, 0);
        rowEnables.set(row, enable);
        if (enable) {
            rowColours.set(row, Color.WHITE);
        } else {
            rowColours.set(row, DISABLED_COLOR);
            ((ChipsManager) getValueAt(row, 2)).reset();
        }
        setValueAt(Pizza.Size.Median, row, 3);
        setValueAt(Pizza.Dough.Thin, row, 4);
        fireTableRowsUpdated(row, row);
    }

    public Color getRowColour(int row) {
        return rowColours.get(row);
    }
}
