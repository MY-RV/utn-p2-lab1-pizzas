/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */

package App.Frames;

import App.Controllers.Auth.LoginController;
import App.Controllers.OrderController;
import App.Frames.Components.IngredientsComponent;
import App.Lib.ChipsManager;
import App.Lib.OrderTableModel;
import App.Lib.Pizza;
import App.Models.Order;
import FBMY.navigation.Frame;
import FBMY.navigation.Kangaroo;
import FBMY.types.Request;
import FBMY.types.SimpleJSON;
import java.util.List;
import java.util.stream.IntStream;
import javax.swing.table.DefaultTableModel;
import java.awt.Component;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Yor
 */
public class OrderFrame extends Frame<OrderController>{
    public OrderFrame() { super(); initComponents(); }
    Map<Integer, Float> pizzaTablePrices = new HashMap();
    int[] OrderOfTheDay = new int[3];
    
    @Override
    public void setUp() {
        setSize(800, 360);
        this.buildTable();
        OrderOfTheDay[0] = (int) Math.floor(Math.random() * 4);
        do { OrderOfTheDay[1] = (int) Math.floor(Math.random() * 4); } 
        while (OrderOfTheDay[0] == OrderOfTheDay[1]);
        OrderOfTheDay[2] = (int) Math.floor(Math.random() * 4) - 1;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BgLayout = new javax.swing.JLayeredPane();
        Bglbl = new javax.swing.JLabel();
        Bglbl1 = new javax.swing.JLabel();
        Bglbl2 = new javax.swing.JLabel();
        Bglbl3 = new javax.swing.JLabel();
        Bglbl4 = new javax.swing.JLabel();
        Bglbl5 = new javax.swing.JLabel();
        Bglbl6 = new javax.swing.JLabel();
        Bglbl7 = new javax.swing.JLabel();
        Bglbl8 = new javax.swing.JLabel();
        Bglbl9 = new javax.swing.JLabel();
        Bglbl10 = new javax.swing.JLabel();
        Bglbl11 = new javax.swing.JLabel();
        Bglbl12 = new javax.swing.JLabel();
        Bglbl13 = new javax.swing.JLabel();
        Bglbl14 = new javax.swing.JLabel();
        Bglbl15 = new javax.swing.JLabel();
        Bglbl16 = new javax.swing.JLabel();
        Bglbl17 = new javax.swing.JLabel();
        Bglbl18 = new javax.swing.JLabel();
        Bglbl19 = new javax.swing.JLabel();
        Bglbl20 = new javax.swing.JLabel();
        Bglbl21 = new javax.swing.JLabel();
        Bglbl22 = new javax.swing.JLabel();
        Bglbl23 = new javax.swing.JLabel();
        Bglbl24 = new javax.swing.JLabel();
        Bglbl25 = new javax.swing.JLabel();
        Bglbl26 = new javax.swing.JLabel();
        Bglbl27 = new javax.swing.JLabel();
        Bglbl28 = new javax.swing.JLabel();
        Bglbl29 = new javax.swing.JLabel();
        Bglbl30 = new javax.swing.JLabel();
        Bglbl31 = new javax.swing.JLabel();
        BodyLayout = new javax.swing.JLayeredPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        Table = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        TblAddons = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        TxtDiscountCode = new javax.swing.JTextField();
        ChbDayPrmotion = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        TxtFinalPrice = new javax.swing.JTextField();
        TxtRawPrice = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        BtnLogout = new javax.swing.JButton();
        BtnMakeMyOrder = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        BgLayout.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Bglbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 0, 100, 100));

        Bglbl1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 100, 100));

        Bglbl2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 0, 100, 100));

        Bglbl3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl3, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 0, 100, 100));

        Bglbl4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl4, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 0, 100, 100));

        Bglbl5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl5, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 0, 100, 100));

        Bglbl6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl6, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 0, 100, 100));

        Bglbl7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl7, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 0, 100, 100));

        Bglbl8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl8, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 100, 100, 100));

        Bglbl9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 100, 100));

        Bglbl10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl10, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 100, 100, 100));

        Bglbl11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl11, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 100, 100, 100));

        Bglbl12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl12, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 100, 100, 100));

        Bglbl13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl13, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 100, 100, 100));

        Bglbl14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl14, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 100, 100, 100));

        Bglbl15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl15, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 100, 100, 100));

        Bglbl16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl16, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 200, 100, 100));

        Bglbl17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl17, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 200, 100, 100));

        Bglbl18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl18, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 200, 100, 100));

        Bglbl19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl19, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 200, 100, 100));

        Bglbl20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl20, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 200, 100, 100));

        Bglbl21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl21, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 200, 100, 100));

        Bglbl22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl22, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 200, 100, 100));

        Bglbl23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl23, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 200, 100, 100));

        Bglbl24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl24, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 300, 100, 100));

        Bglbl25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl25, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 300, 100, 100));

        Bglbl26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl26, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 300, 100, 100));

        Bglbl27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl27, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 300, 100, 100));

        Bglbl28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl28, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 300, 100, 100));

        Bglbl29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl29, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 300, 100, 100));

        Bglbl30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl30, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 300, 100, 100));

        Bglbl31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl31, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 300, 100, 100));

        BodyLayout.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Table.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Table.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        Table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "IN", "Type", "Ingredients", "Size", "Dough", "Price"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        Table.setRequestFocusEnabled(false);
        Table.setRowHeight(26);
        Table.setRowSelectionAllowed(false);
        Table.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(Table);

        BodyLayout.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, 740, 130));

        TblAddons.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        TblAddons.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "IN", "Addon", "Price"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TblAddons.setRowHeight(25);
        TblAddons.setRowSelectionAllowed(false);
        TblAddons.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(TblAddons);

        BodyLayout.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 180, 200, 100));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 4, true));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Discount Code");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, 30));

        TxtDiscountCode.setEditable(false);
        TxtDiscountCode.setBackground(java.awt.SystemColor.control);
        TxtDiscountCode.setFont(new java.awt.Font("Comic Sans MS", 0, 16)); // NOI18N
        TxtDiscountCode.setForeground(new java.awt.Color(51, 51, 51));
        TxtDiscountCode.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtDiscountCode.setBorder(null);
        jPanel1.add(TxtDiscountCode, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 10, 120, 30));

        ChbDayPrmotion.setBackground(new java.awt.Color(255, 255, 255));
        ChbDayPrmotion.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        ChbDayPrmotion.setForeground(new java.awt.Color(51, 51, 51));
        ChbDayPrmotion.setText("Order the Day's Promotion");
        ChbDayPrmotion.setFocusPainted(false);
        ChbDayPrmotion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChbDayPrmotionActionPerformed(evt);
            }
        });
        jPanel1.add(ChbDayPrmotion, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, -1, 30));

        BodyLayout.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 180, 260, 100));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 4, true));
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("Final Price");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, -1, 30));

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setText("Raw Price");
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, 30));

        TxtFinalPrice.setEditable(false);
        TxtFinalPrice.setBackground(java.awt.SystemColor.control);
        TxtFinalPrice.setFont(new java.awt.Font("Comic Sans MS", 0, 16)); // NOI18N
        TxtFinalPrice.setForeground(new java.awt.Color(51, 51, 51));
        TxtFinalPrice.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TxtFinalPrice.setText("0.0");
        TxtFinalPrice.setBorder(null);
        jPanel2.add(TxtFinalPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, 100, 30));

        TxtRawPrice.setEditable(false);
        TxtRawPrice.setBackground(java.awt.SystemColor.control);
        TxtRawPrice.setFont(new java.awt.Font("Comic Sans MS", 0, 16)); // NOI18N
        TxtRawPrice.setForeground(new java.awt.Color(51, 51, 51));
        TxtRawPrice.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TxtRawPrice.setText("0.0");
        TxtRawPrice.setBorder(null);
        jPanel2.add(TxtRawPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 100, 30));

        jPanel3.setBackground(java.awt.SystemColor.control);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        jPanel2.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 10, 10, 30));

        jPanel4.setBackground(java.awt.SystemColor.control);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        jPanel2.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 60, -1, 30));

        BodyLayout.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 180, 220, 100));

        BtnLogout.setBackground(java.awt.SystemColor.control);
        BtnLogout.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        BtnLogout.setText("Close Session");
        BtnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnLogoutActionPerformed(evt);
            }
        });
        BodyLayout.add(BtnLogout, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 300, -1, 40));

        BtnMakeMyOrder.setBackground(java.awt.SystemColor.control);
        BtnMakeMyOrder.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        BtnMakeMyOrder.setText("Make my Order");
        BtnMakeMyOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnMakeMyOrderActionPerformed(evt);
            }
        });
        BodyLayout.add(BtnMakeMyOrder, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 300, -1, 40));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(BodyLayout, javax.swing.GroupLayout.PREFERRED_SIZE, 800, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(BgLayout))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BodyLayout, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(BgLayout))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ChbDayPrmotionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChbDayPrmotionActionPerformed
        toogleOrderOfTheDay();
    }//GEN-LAST:event_ChbDayPrmotionActionPerformed

    private void BtnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnLogoutActionPerformed
        Kangaroo.goTo(LoginController.class);
    }//GEN-LAST:event_BtnLogoutActionPerformed

    private void BtnMakeMyOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnMakeMyOrderActionPerformed
        var payload = genPayload();
        controller.makeOrder(payload);
    }//GEN-LAST:event_BtnMakeMyOrderActionPerformed

    private void toogleOrderOfTheDay() {
        var model = (DefaultTableModel) Table.getModel();
        for (int i = 0; i < Table.getRowCount(); i++) 
            model.setValueAt(false, i, 0);
        var addonsModel = (DefaultTableModel) TblAddons.getModel();
        for (int i = 0; i < TblAddons.getRowCount(); i++) 
            addonsModel.setValueAt(false, i, 0);
        Table.setEnabled(!ChbDayPrmotion.isSelected());
        TblAddons.setEnabled(!ChbDayPrmotion.isSelected());
        
        if (!ChbDayPrmotion.isSelected()) return;
        model.setValueAt(true, OrderOfTheDay[0], 0);
        model.setValueAt(Pizza.Size.Big, OrderOfTheDay[0], OrderTableModel.Columns.Size.ordinal());
        model.setValueAt(true, OrderOfTheDay[1], 0);
        model.setValueAt(Pizza.Dough.Thick, OrderOfTheDay[1], OrderTableModel.Columns.Dough.ordinal());
        
        if (OrderOfTheDay[2] == -1) return;
        addonsModel.setValueAt(true, OrderOfTheDay[2], 0);
    }
    
    private void buildTable() {
        Table.setModel(new OrderTableModel());
        var model = (OrderTableModel) Table.getModel();
        
        var CbxSize = new JComboBox();
        Arrays.asList(Pizza.Size.values()).forEach(CbxSize::addItem);
        Table.getColumnModel().getColumn(
            OrderTableModel.Columns.Size.ordinal()
        ).setCellEditor(new DefaultCellEditor(CbxSize));
        
        var CbxDough = new JComboBox();
        Arrays.asList(Pizza.Dough.values()).forEach(CbxDough::addItem);
        Table.getColumnModel().getColumn(
            OrderTableModel.Columns.Dough.ordinal()
        ).setCellEditor(new DefaultCellEditor(CbxDough));
        
        var widths = List.of(25, 85, 380, 65, 55);
        IntStream.range(0, widths.size()).forEach(c -> {
            var width = widths.get(c);
            Table.getColumnModel().getColumn(c).setMaxWidth(width);
            Table.getColumnModel().getColumn(c).setMinWidth(width);
        });
        
        IntStream.range(0, Table.getColumnCount()).forEach(c -> {
            if (c == 0) return;
            Table.getColumnModel().getColumn(c).setCellRenderer(
                new DefaultTableCellRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        var model = (OrderTableModel) table.getModel();
                        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        c.setBackground(model.getRowColour(row));
                        return c;
                    }
                }
            );
        });
        
        var origin = this;
        Table.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override public void mouseClicked(java.awt.event.MouseEvent evt) {
                int row = Table.rowAtPoint(evt.getPoint());
                int col = Table.columnAtPoint(evt.getPoint());
                if (!(Boolean)  model.getValueAt(row, 0)) return;
                if (col == 2) Kangaroo.sideGet(
                    "IManager", IngredientsComponent.Component.class
                ).frame.Mount(origin, (ChipsManager) model.getValueAt(row, 2),
                    (response) -> { 
                        recalculateRawPrice(); 
                        model.setValueAt(response, row, col); 
                    }
                );
            }
        });
        

        model.addTableModelListener((e) -> {
            int column = e.getColumn();
            int row = e.getFirstRow();
            switch (column) {
                case 0 -> model.reloadRow(row);
                case 3 ->  model.recalculate(row);
                case 4 ->  model.recalculate(row);
            }
            if ((Boolean) model.getValueAt(row, 0)) {
                pizzaTablePrices.put(row, 
                    (Float) model.getValueAt(row, OrderTableModel.Columns.Price.ordinal())
                );
            } else pizzaTablePrices.put(row, 0f);
            recalculateRawPrice();
        });
        
        var addonsModel = (DefaultTableModel) TblAddons.getModel();
        addonsModel.setRowCount(0);
        for (var addon : Order.Addon.values())
            addonsModel.addRow(new Object[]{false, addon, addon.price});
        TblAddons.getColumnModel().getColumn(0).setMaxWidth(25);
        TblAddons.getColumnModel().getColumn(0).setMinWidth(25);
        TblAddons.getColumnModel().getColumn(1).setMaxWidth(100);
        TblAddons.getColumnModel().getColumn(1).setMinWidth(100);
        
        TblAddons.getModel().addTableModelListener((e) -> recalculateRawPrice());
    }
    
    public void recalculateRawPrice() {
        Float price = 0f;
        for (var entry : pizzaTablePrices.entrySet()) price += entry.getValue();
        var addonsModel = (DefaultTableModel) TblAddons.getModel();
        for (int i = 0; i < TblAddons.getRowCount(); i++) {
            if (!(Boolean) addonsModel.getValueAt(i, 0)) continue;
            price += (Integer) addonsModel.getValueAt(i, 2);
        } TxtRawPrice.setText(price.toString());
        recalculateFinalPrice(price);
    }
    
    void recalculateFinalPrice(Float rawPrice) {
        var discountCode = checkDiscountCodeAt(0);
        if (discountCode == null) 
            discountCode = checkDiscountCodeAt(2);
        if (discountCode != null) {
            var existingCode = TxtDiscountCode.getText();
            if (existingCode.isBlank()) 
                TxtDiscountCode.setText(discountCode);
            else TxtDiscountCode.setText(existingCode);
            rawPrice *= .95f;
        } else TxtDiscountCode.setText(null);
        TxtFinalPrice.setText(rawPrice.toString());
    }
    
    String checkDiscountCodeAt(int row) {
        if (ChbDayPrmotion.isSelected()) return null;
        var model = (OrderTableModel) Table.getModel();
        if (!(Boolean) model.getValueAt(row, 0)) return null;
        var size = model.getValueAt(row, OrderTableModel.Columns.Size.ordinal());
        if (size != Pizza.Size.Familiar) return null;
        for (var entry : ((ChipsManager) model.getValueAt(row, 
            OrderTableModel.Columns.Ingredients.ordinal())
        ).entrySet()) if (!entry.getValue()) return null;
        return newDiscountCode();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLayeredPane BgLayout;
    private javax.swing.JLabel Bglbl;
    private javax.swing.JLabel Bglbl1;
    private javax.swing.JLabel Bglbl10;
    private javax.swing.JLabel Bglbl11;
    private javax.swing.JLabel Bglbl12;
    private javax.swing.JLabel Bglbl13;
    private javax.swing.JLabel Bglbl14;
    private javax.swing.JLabel Bglbl15;
    private javax.swing.JLabel Bglbl16;
    private javax.swing.JLabel Bglbl17;
    private javax.swing.JLabel Bglbl18;
    private javax.swing.JLabel Bglbl19;
    private javax.swing.JLabel Bglbl2;
    private javax.swing.JLabel Bglbl20;
    private javax.swing.JLabel Bglbl21;
    private javax.swing.JLabel Bglbl22;
    private javax.swing.JLabel Bglbl23;
    private javax.swing.JLabel Bglbl24;
    private javax.swing.JLabel Bglbl25;
    private javax.swing.JLabel Bglbl26;
    private javax.swing.JLabel Bglbl27;
    private javax.swing.JLabel Bglbl28;
    private javax.swing.JLabel Bglbl29;
    private javax.swing.JLabel Bglbl3;
    private javax.swing.JLabel Bglbl30;
    private javax.swing.JLabel Bglbl31;
    private javax.swing.JLabel Bglbl4;
    private javax.swing.JLabel Bglbl5;
    private javax.swing.JLabel Bglbl6;
    private javax.swing.JLabel Bglbl7;
    private javax.swing.JLabel Bglbl8;
    private javax.swing.JLabel Bglbl9;
    private javax.swing.JLayeredPane BodyLayout;
    private javax.swing.JButton BtnLogout;
    private javax.swing.JButton BtnMakeMyOrder;
    private javax.swing.JCheckBox ChbDayPrmotion;
    private javax.swing.JTable Table;
    private javax.swing.JTable TblAddons;
    private javax.swing.JTextField TxtDiscountCode;
    private javax.swing.JTextField TxtFinalPrice;
    private javax.swing.JTextField TxtRawPrice;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables

    public String newDiscountCode() {
        Random random = new Random();
        return random.ints(97, 123).limit(6).collect(
            StringBuilder::new,
            StringBuilder::appendCodePoint, 
            StringBuilder::append
        ).toString().toUpperCase();
    }
    
    Request genPayload() {
        var payload = new Request();
        var model = (DefaultTableModel) Table.getModel();
        var addonModel = (DefaultTableModel) TblAddons.getModel();
        
        var pizzas = new SimpleJSON('\'');
        for (int i = 0; i < Table.getRowCount(); i++) {
            if (!(Boolean) model.getValueAt(i, 0)) continue;
            var pizza = new HashMap();
            pizza.put("type", 
                model.getValueAt(i, OrderTableModel.Columns.Type.ordinal()).toString());
            pizza.put("ingredients", 
                model.getValueAt(i, OrderTableModel.Columns.Ingredients.ordinal()).toString());
            pizza.put("size", 
                model.getValueAt(i, OrderTableModel.Columns.Size.ordinal()).toString());
            pizza.put("dough", 
                model.getValueAt(i, OrderTableModel.Columns.Dough.ordinal()).toString());
            pizza.put("price", 
                model.getValueAt(i, OrderTableModel.Columns.Price.ordinal()).toString());
            pizzas.add(pizza);
        }
        
        var addons = new Order.AddonsList();
        for (int i = 0; i < TblAddons.getRowCount(); i++) {
            if (!(Boolean) addonModel.getValueAt(i, 0)) continue;
            addons.add((Order.Addon) addonModel.getValueAt(i, 1));
        }
        
        payload.put("pizzas", pizzas.toString());
        payload.put("addons", addons);
        payload.put("discount_code", TxtDiscountCode.getText());
        payload.put("raw_price", TxtRawPrice.getText());
        payload.put("final_price", TxtFinalPrice.getText());
        return payload;
    }
    
}
