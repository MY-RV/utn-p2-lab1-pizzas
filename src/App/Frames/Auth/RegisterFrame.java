/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package App.Frames.Auth;

import App.Controllers.Auth.LoginController;
import App.Controllers.Auth.RegisterController;
import App.State;
import FBMY.navigation.Frame;
import FBMY.navigation.Kangaroo;
import FBMY.types.Request;

/**
 *
 * @author Yor
 */
public class RegisterFrame extends Frame<RegisterController> {
    public RegisterFrame() { super(); initComponents(); }

    @Override
    public void setUp() {
        setResizable(false);
        setSize(400, 500);
        State.Provinces.forEach(CbxProvince::addItem);
        CbxProvince.setSelectedIndex(-1);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        GrpGender = new javax.swing.ButtonGroup();
        BodyLayout = new javax.swing.JLayeredPane();
        LblSubtitle = new javax.swing.JLabel();
        LblTitle = new javax.swing.JLabel();
        PnlLogin = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        BtnLogin = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        PnlRegister = new javax.swing.JPanel();
        BtnRegister = new javax.swing.JButton();
        PnlGender = new javax.swing.JPanel();
        LblGender = new javax.swing.JLabel();
        RdbOther = new javax.swing.JRadioButton();
        RdbMale = new javax.swing.JRadioButton();
        RdbFemale = new javax.swing.JRadioButton();
        PnlProvince = new javax.swing.JPanel();
        LblProvince = new javax.swing.JLabel();
        PnlInptProvince = new javax.swing.JPanel();
        CbxProvince = new javax.swing.JComboBox<>();
        PnlName = new javax.swing.JPanel();
        LblName = new javax.swing.JLabel();
        PnlInptName = new javax.swing.JPanel();
        TxtName = new javax.swing.JTextField();
        PnlIdentification = new javax.swing.JPanel();
        LblIdentification = new javax.swing.JLabel();
        PnlInptIdentification = new javax.swing.JPanel();
        TxtIdentification = new javax.swing.JTextField();
        BgLayer = new javax.swing.JLayeredPane();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        BodyLayout.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblSubtitle.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        LblSubtitle.setForeground(new java.awt.Color(153, 153, 153));
        LblSubtitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblSubtitle.setText("REGISTRATION");
        LblSubtitle.setToolTipText("");
        BodyLayout.add(LblSubtitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 90, 400, 30));

        LblTitle.setFont(new java.awt.Font("Comic Sans MS", 1, 48)); // NOI18N
        LblTitle.setForeground(new java.awt.Color(255, 255, 255));
        LblTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblTitle.setText("JVM PIZZA");
        LblTitle.setToolTipText("");
        BodyLayout.add(LblTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 36, 400, 60));

        PnlLogin.setBackground(new java.awt.Color(51, 51, 51));
        PnlLogin.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 51, 51), 1, true));
        PnlLogin.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel24.setBackground(new java.awt.Color(255, 255, 255));
        jLabel24.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(153, 153, 153));
        jLabel24.setText("Got and Account? maybe want to");
        PnlLogin.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 220, -1));

        BtnLogin.setBackground(new java.awt.Color(51, 51, 51));
        BtnLogin.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        BtnLogin.setForeground(new java.awt.Color(204, 204, 204));
        BtnLogin.setText("Log in");
        BtnLogin.setBorder(null);
        BtnLogin.setBorderPainted(false);
        BtnLogin.setFocusPainted(false);
        BtnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnLoginActionPerformed(evt);
            }
        });
        PnlLogin.add(BtnLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 0, 60, 40));

        BodyLayout.add(PnlLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 430, -1, 40));

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));
        jPanel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 10, true));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        PnlRegister.setBackground(new java.awt.Color(255, 255, 255));
        PnlRegister.setForeground(new java.awt.Color(51, 51, 51));
        PnlRegister.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        BtnRegister.setBackground(java.awt.SystemColor.control);
        BtnRegister.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        BtnRegister.setForeground(new java.awt.Color(51, 51, 51));
        BtnRegister.setText("That's it register me");
        BtnRegister.setBorder(new javax.swing.border.LineBorder(java.awt.SystemColor.control, 5, true));
        BtnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegisterActionPerformed(evt);
            }
        });
        PnlRegister.add(BtnRegister, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 0, 220, 40));

        jPanel1.add(PnlRegister, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 320, 50));

        PnlGender.setBackground(new java.awt.Color(255, 255, 255));
        PnlGender.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblGender.setBackground(new java.awt.Color(255, 255, 255));
        LblGender.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        LblGender.setForeground(new java.awt.Color(51, 51, 51));
        LblGender.setText("Gender");
        PnlGender.add(LblGender, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 70, 30));

        RdbOther.setBackground(new java.awt.Color(255, 255, 255));
        GrpGender.add(RdbOther);
        RdbOther.setFont(new java.awt.Font("Comic Sans MS", 0, 16)); // NOI18N
        RdbOther.setForeground(new java.awt.Color(51, 51, 51));
        RdbOther.setText("Other");
        RdbOther.setBorder(null);
        PnlGender.add(RdbOther, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 10, -1, 30));

        RdbMale.setBackground(new java.awt.Color(255, 255, 255));
        GrpGender.add(RdbMale);
        RdbMale.setFont(new java.awt.Font("Comic Sans MS", 0, 16)); // NOI18N
        RdbMale.setForeground(new java.awt.Color(51, 51, 51));
        RdbMale.setText("Male");
        RdbMale.setBorder(null);
        PnlGender.add(RdbMale, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, -1, 30));

        RdbFemale.setBackground(new java.awt.Color(255, 255, 255));
        GrpGender.add(RdbFemale);
        RdbFemale.setFont(new java.awt.Font("Comic Sans MS", 0, 16)); // NOI18N
        RdbFemale.setForeground(new java.awt.Color(51, 51, 51));
        RdbFemale.setText("Female");
        RdbFemale.setBorder(null);
        PnlGender.add(RdbFemale, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 10, -1, 30));

        jPanel1.add(PnlGender, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 160, 320, 50));

        PnlProvince.setBackground(new java.awt.Color(255, 255, 255));
        PnlProvince.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblProvince.setBackground(new java.awt.Color(255, 255, 255));
        LblProvince.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        LblProvince.setForeground(new java.awt.Color(51, 51, 51));
        LblProvince.setText("Province");
        PnlProvince.add(LblProvince, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 70, 30));

        PnlInptProvince.setBackground(java.awt.SystemColor.control);
        PnlInptProvince.setForeground(new java.awt.Color(51, 51, 51));
        PnlInptProvince.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        CbxProvince.setBackground(java.awt.SystemColor.control);
        CbxProvince.setFont(new java.awt.Font("Comic Sans MS", 0, 16)); // NOI18N
        CbxProvince.setForeground(new java.awt.Color(51, 51, 51));
        CbxProvince.setToolTipText("Select a value");
        CbxProvince.setBorder(null);
        CbxProvince.setLightWeightPopupEnabled(false);
        CbxProvince.setRequestFocusEnabled(false);
        PnlInptProvince.add(CbxProvince, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 200, 30));

        PnlProvince.add(PnlInptProvince, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, 220, 30));

        jPanel1.add(PnlProvince, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 320, 50));

        PnlName.setBackground(new java.awt.Color(255, 255, 255));
        PnlName.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblName.setBackground(new java.awt.Color(255, 255, 255));
        LblName.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        LblName.setForeground(new java.awt.Color(51, 51, 51));
        LblName.setText("Name");
        PnlName.add(LblName, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 70, 30));

        PnlInptName.setBackground(java.awt.SystemColor.control);
        PnlInptName.setForeground(new java.awt.Color(51, 51, 51));
        PnlInptName.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TxtName.setBackground(java.awt.SystemColor.control);
        TxtName.setFont(new java.awt.Font("Comic Sans MS", 0, 16)); // NOI18N
        TxtName.setForeground(new java.awt.Color(51, 51, 51));
        TxtName.setBorder(null);
        PnlInptName.add(TxtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 200, 30));

        PnlName.add(PnlInptName, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, 220, 30));

        jPanel1.add(PnlName, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 320, 50));

        PnlIdentification.setBackground(new java.awt.Color(255, 255, 255));
        PnlIdentification.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblIdentification.setBackground(new java.awt.Color(255, 255, 255));
        LblIdentification.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        LblIdentification.setForeground(new java.awt.Color(51, 51, 51));
        LblIdentification.setText("Id Card");
        PnlIdentification.add(LblIdentification, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 70, 30));

        PnlInptIdentification.setBackground(java.awt.SystemColor.control);
        PnlInptIdentification.setForeground(new java.awt.Color(51, 51, 51));
        PnlInptIdentification.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TxtIdentification.setBackground(java.awt.SystemColor.control);
        TxtIdentification.setFont(new java.awt.Font("Comic Sans MS", 0, 16)); // NOI18N
        TxtIdentification.setForeground(new java.awt.Color(51, 51, 51));
        TxtIdentification.setBorder(null);
        PnlInptIdentification.add(TxtIdentification, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 200, 30));

        PnlIdentification.add(PnlInptIdentification, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, 220, 30));

        jPanel1.add(PnlIdentification, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 320, 50));

        BodyLayout.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, 340, 270));

        BgLayer.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 100, 100, -1));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 100, -1));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 0, 100, -1));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 200, 100, -1));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 100, -1));

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 0, 100, -1));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 0, 100, -1));

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 100, 100, -1));

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 100, 100, -1));

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 200, 100, -1));

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 200, 100, -1));

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 200, 100, -1));

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 300, 100, -1));

        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 300, 100, -1));

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 300, 100, -1));

        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 300, 100, -1));

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 400, 100, -1));

        jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 400, 100, -1));

        jLabel22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 400, 100, -1));

        jLabel23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 400, 100, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BodyLayout, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(BgLayer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BodyLayout, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(BgLayer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnLoginActionPerformed
        Kangaroo.goTo(LoginController.class);
    }//GEN-LAST:event_BtnLoginActionPerformed

    private void BtnRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegisterActionPerformed
        controller.register(genPayload());
    }//GEN-LAST:event_BtnRegisterActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLayeredPane BgLayer;
    private javax.swing.JLayeredPane BodyLayout;
    private javax.swing.JButton BtnLogin;
    private javax.swing.JButton BtnRegister;
    private javax.swing.JComboBox<String> CbxProvince;
    private javax.swing.ButtonGroup GrpGender;
    private javax.swing.JLabel LblGender;
    private javax.swing.JLabel LblIdentification;
    private javax.swing.JLabel LblName;
    private javax.swing.JLabel LblProvince;
    private javax.swing.JLabel LblSubtitle;
    private javax.swing.JLabel LblTitle;
    private javax.swing.JPanel PnlGender;
    private javax.swing.JPanel PnlIdentification;
    private javax.swing.JPanel PnlInptIdentification;
    private javax.swing.JPanel PnlInptName;
    private javax.swing.JPanel PnlInptProvince;
    private javax.swing.JPanel PnlLogin;
    private javax.swing.JPanel PnlName;
    private javax.swing.JPanel PnlProvince;
    private javax.swing.JPanel PnlRegister;
    private javax.swing.JRadioButton RdbFemale;
    private javax.swing.JRadioButton RdbMale;
    private javax.swing.JRadioButton RdbOther;
    private javax.swing.JTextField TxtIdentification;
    private javax.swing.JTextField TxtName;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

    private Request genPayload() {
        var payload = new Request();
        payload.put("identification", getValue(TxtIdentification));
        payload.put("name", getValue(TxtName));
        payload.put("province", getValue(CbxProvince));
        payload.put("gender", getValue(GrpGender));
        return payload;
    }
    
}
