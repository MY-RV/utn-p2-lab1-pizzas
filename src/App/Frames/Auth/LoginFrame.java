/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package App.Frames.Auth;

import App.Controllers.Auth.LoginController;
import App.Controllers.Auth.RegisterController;
import FBMY.navigation.Frame;
import FBMY.navigation.Kangaroo;

/**
 *
 * @author Yor
 */
public class LoginFrame extends Frame<LoginController> {
    public LoginFrame() { super(); initComponents(); }

    @Override
    public void setUp() {
        setResizable(false);
        setSize(400, 500);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panel = new javax.swing.JPanel();
        LblTitle = new javax.swing.JLabel();
        BodyLayout = new javax.swing.JLayeredPane();
        PnlFormBorder = new javax.swing.JPanel();
        PnlForm = new javax.swing.JPanel();
        LblIdentification = new javax.swing.JLabel();
        LblPizzaIco1 = new javax.swing.JLabel();
        PnlIdentification = new javax.swing.JPanel();
        TxtIdentification = new javax.swing.JTextField();
        BtnSignIn = new javax.swing.JButton();
        LblPizzaIco = new javax.swing.JLabel();
        PnlRegister = new javax.swing.JPanel();
        LblRegister = new javax.swing.JLabel();
        BtnRegister = new javax.swing.JButton();
        LblSubtitle = new javax.swing.JLabel();
        BgLayer = new javax.swing.JLayeredPane();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Panel.setBackground(new java.awt.Color(255, 255, 255));
        Panel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblTitle.setFont(new java.awt.Font("Comic Sans MS", 1, 48)); // NOI18N
        LblTitle.setForeground(new java.awt.Color(255, 255, 255));
        LblTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblTitle.setText("JVM PIZZA");
        LblTitle.setToolTipText("");
        Panel.add(LblTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 36, 400, 60));

        BodyLayout.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        PnlFormBorder.setBackground(new java.awt.Color(51, 51, 51));
        PnlFormBorder.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 10, true));
        PnlFormBorder.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        PnlForm.setBackground(new java.awt.Color(255, 255, 255));
        PnlForm.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblIdentification.setBackground(new java.awt.Color(51, 51, 51));
        LblIdentification.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        LblIdentification.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblIdentification.setText("Identification");
        PnlForm.add(LblIdentification, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 280, -1));

        LblPizzaIco1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/pizza-lt.jpg"))); // NOI18N
        PnlForm.add(LblPizzaIco1, new org.netbeans.lib.awtextra.AbsoluteConstraints(-10, -10, 160, -1));

        PnlIdentification.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TxtIdentification.setBackground(java.awt.SystemColor.control);
        TxtIdentification.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        TxtIdentification.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtIdentification.setBorder(null);
        PnlIdentification.add(TxtIdentification, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 260, 30));

        PnlForm.add(PnlIdentification, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 280, 30));

        BtnSignIn.setBackground(java.awt.SystemColor.control);
        BtnSignIn.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        BtnSignIn.setText("Sign In");
        BtnSignIn.setBorder(new javax.swing.border.LineBorder(java.awt.SystemColor.control, 5, true));
        BtnSignIn.setFocusPainted(false);
        BtnSignIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSignInActionPerformed(evt);
            }
        });
        PnlForm.add(BtnSignIn, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 110, 100, 40));

        PnlFormBorder.add(PnlForm, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 300, 170));

        LblPizzaIco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/pizza-lt.jpg"))); // NOI18N
        PnlFormBorder.add(LblPizzaIco, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 160, -1));

        BodyLayout.add(PnlFormBorder, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 140, 320, 190));

        PnlRegister.setBackground(new java.awt.Color(51, 51, 51));
        PnlRegister.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 51, 51), 1, true));
        PnlRegister.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblRegister.setBackground(new java.awt.Color(255, 255, 255));
        LblRegister.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        LblRegister.setForeground(new java.awt.Color(153, 153, 153));
        LblRegister.setText("New Here? just go and ");
        PnlRegister.add(LblRegister, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 160, -1));

        BtnRegister.setBackground(new java.awt.Color(51, 51, 51));
        BtnRegister.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        BtnRegister.setForeground(new java.awt.Color(204, 204, 204));
        BtnRegister.setText("Register");
        BtnRegister.setBorder(null);
        BtnRegister.setBorderPainted(false);
        BtnRegister.setFocusPainted(false);
        BtnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegisterActionPerformed(evt);
            }
        });
        PnlRegister.add(BtnRegister, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 0, 90, 40));

        BodyLayout.add(PnlRegister, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 430, 250, 40));

        LblSubtitle.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        LblSubtitle.setForeground(new java.awt.Color(153, 153, 153));
        LblSubtitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblSubtitle.setText("Sign In");
        LblSubtitle.setToolTipText("");
        BodyLayout.add(LblSubtitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 80, 400, 50));

        Panel.add(BodyLayout, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 500));

        BgLayer.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 100, 100, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 100, -1));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 0, 100, -1));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 200, 100, -1));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 100, -1));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 0, 100, -1));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 0, 100, -1));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 100, 100, -1));

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 100, 100, -1));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 200, 100, -1));

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 200, 100, -1));

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 200, 100, -1));

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 300, 100, -1));

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 300, 100, -1));

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 300, 100, -1));

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 300, 100, -1));

        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 400, 100, -1));

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 400, 100, -1));

        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 400, 100, -1));

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayer.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 400, 100, -1));

        Panel.add(BgLayer, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 500));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegisterActionPerformed
        Kangaroo.goTo(RegisterController.class);
    }//GEN-LAST:event_BtnRegisterActionPerformed

    private void BtnSignInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSignInActionPerformed
        controller.login(getValue(TxtIdentification));
    }//GEN-LAST:event_BtnSignInActionPerformed



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLayeredPane BgLayer;
    private javax.swing.JLayeredPane BodyLayout;
    private javax.swing.JButton BtnRegister;
    private javax.swing.JButton BtnSignIn;
    private javax.swing.JLabel LblIdentification;
    private javax.swing.JLabel LblPizzaIco;
    private javax.swing.JLabel LblPizzaIco1;
    private javax.swing.JLabel LblRegister;
    private javax.swing.JLabel LblSubtitle;
    private javax.swing.JLabel LblTitle;
    private javax.swing.JPanel Panel;
    private javax.swing.JPanel PnlForm;
    private javax.swing.JPanel PnlFormBorder;
    private javax.swing.JPanel PnlIdentification;
    private javax.swing.JPanel PnlRegister;
    private javax.swing.JTextField TxtIdentification;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    // End of variables declaration//GEN-END:variables
}
