package App.Frames.Components;


import App.Frames.OrderFrame;
import App.Lib.ChipsManager;
import FBMY.navigation.Controller;
import FBMY.navigation.Frame;
import FBMY.util.Closures.IRunnable;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */

/**
 *
 * @author Yor
 */
public class IngredientsComponent extends Frame<Controller> {
    public IngredientsComponent() { super(); initComponents(); }
    public class Component extends Controller<IngredientsComponent> {
        public Component(IngredientsComponent frame) { super(frame); }
        @Override public void index() {
            frame.setLocationRelativeTo(null);
            frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            frame.setVisible(true); 
        }
    }
    
    public void Mount(OrderFrame origin, ChipsManager manager, IRunnable<ChipsManager> callback){
        Table.getColumnModel().getColumn(0).setMaxWidth(25);
        Table.getColumnModel().getColumn(0).setMinWidth(25);
        var model = (DefaultTableModel) Table.getModel(); model.setRowCount(0);
        manager.forEach((ingredient, include) -> {
            model.addRow(new Object[]{include, ingredient});
        });
        addWindowListener(new WindowAdapter() {
            @Override public void windowClosing(WindowEvent e) {
                for (int i = 0; i < model.getRowCount(); i++) {
                    var key = (String) model.getValueAt(i, 1);
                    var val = (Boolean) model.getValueAt(i, 0);
                    manager.put(key, val);
                } setVisible(false);
                callback.run(manager);
            }
        });
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BodyLayout = new javax.swing.JLayeredPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        Table = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        BodyLayout.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Table.setAutoCreateRowSorter(true);
        Table.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        Table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "IN", "Ingredient"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        Table.setRowHeight(25);
        jScrollPane1.setViewportView(Table);

        BodyLayout.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 240, 180));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BodyLayout, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BodyLayout, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLayeredPane BodyLayout;
    private javax.swing.JTable Table;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
