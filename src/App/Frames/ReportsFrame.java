/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package App.Frames;

import App.Controllers.Auth.LoginController;
import App.Controllers.ReportsController;
import App.Models.Client;
import App.Models.Order;
import FBMY.navigation.Frame;
import FBMY.navigation.Kangaroo;
import FBMY.treats.dataWorkers.Racoon;
import java.util.List;
import java.util.Map;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.TableView;

/**
 *
 * @author Yor
 */
public class ReportsFrame extends Frame<ReportsController> {
    public ReportsFrame() { super(); initComponents(); }
    
    Map<String, Client> clients;
    List<Order> orders;

    @Override
    public void setUp() {
        setSize(800, 400); loading = true;
        clients = controller.getClientsInMap();
        clients.keySet().forEach(CbxClients::addItem);
        CbxClients.setSelectedIndex(-1);
        Table.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override public void mouseClicked(java.awt.event.MouseEvent evt) {
                int row = Table.rowAtPoint(evt.getPoint());
                var orderData = "";
                var order = orders.get(row);
                orderData += "ID: " + order.id() + "\r\n";
                if (!order.pizzas().isEmpty()) {
                    orderData += "Pizzas:\r\n";
                    var pizzaKeys = List.of("type", "ingredients", "size", "dough", "price");
                    for (var pizza : order.pizzas()) {
                        orderData += "    {\r\n";
                        for (var key : pizzaKeys) 
                            orderData += "        " + key + ": " + pizza.get(key) + "\r\n";
                        orderData += "    {\r\n";
                    }
                }
                orderData += "Addons: " + order.addons()+ "\r\n";
                orderData += "Discount: " + order.discount_code()+ "\r\n";
                orderData += "Raw Price: ₡" + order.raw_price()+ "\r\n";
                orderData += "Total: ₡" + order.final_price()+ "\r\n";
                TxtOrderData.setText(orderData);
            }
        });
        loading = false;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BodyLayout = new javax.swing.JLayeredPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        Table = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        TxtOrderData = new javax.swing.JTextArea();
        CbxClients = new javax.swing.JComboBox<>();
        BtnLogOut = new javax.swing.JButton();
        BgLayout = new javax.swing.JLayeredPane();
        Bglbl = new javax.swing.JLabel();
        Bglbl1 = new javax.swing.JLabel();
        Bglbl2 = new javax.swing.JLabel();
        Bglbl3 = new javax.swing.JLabel();
        Bglbl4 = new javax.swing.JLabel();
        Bglbl5 = new javax.swing.JLabel();
        Bglbl6 = new javax.swing.JLabel();
        Bglbl7 = new javax.swing.JLabel();
        Bglbl8 = new javax.swing.JLabel();
        Bglbl9 = new javax.swing.JLabel();
        Bglbl10 = new javax.swing.JLabel();
        Bglbl11 = new javax.swing.JLabel();
        Bglbl12 = new javax.swing.JLabel();
        Bglbl13 = new javax.swing.JLabel();
        Bglbl14 = new javax.swing.JLabel();
        Bglbl15 = new javax.swing.JLabel();
        Bglbl16 = new javax.swing.JLabel();
        Bglbl17 = new javax.swing.JLabel();
        Bglbl18 = new javax.swing.JLabel();
        Bglbl19 = new javax.swing.JLabel();
        Bglbl20 = new javax.swing.JLabel();
        Bglbl21 = new javax.swing.JLabel();
        Bglbl22 = new javax.swing.JLabel();
        Bglbl23 = new javax.swing.JLabel();
        Bglbl24 = new javax.swing.JLabel();
        Bglbl25 = new javax.swing.JLabel();
        Bglbl26 = new javax.swing.JLabel();
        Bglbl27 = new javax.swing.JLabel();
        Bglbl28 = new javax.swing.JLabel();
        Bglbl29 = new javax.swing.JLabel();
        Bglbl30 = new javax.swing.JLabel();
        Bglbl31 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        BodyLayout.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Table.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        Table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Gender", "Province", "Order_Id"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        Table.setRowHeight(25);
        jScrollPane1.setViewportView(Table);

        BodyLayout.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, -1, 330));

        TxtOrderData.setEditable(false);
        TxtOrderData.setColumns(20);
        TxtOrderData.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        TxtOrderData.setRows(5);
        jScrollPane2.setViewportView(TxtOrderData);

        BodyLayout.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 20, 280, 360));

        CbxClients.setBackground(new java.awt.Color(255, 255, 254));
        CbxClients.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        CbxClients.setBorder(null);
        CbxClients.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CbxClientsActionPerformed(evt);
            }
        });
        BodyLayout.add(CbxClients, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 20, 370, 30));

        BtnLogOut.setBackground(new java.awt.Color(255, 255, 254));
        BtnLogOut.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        BtnLogOut.setText("Log Out");
        BtnLogOut.setBorder(null);
        BtnLogOut.setFocusPainted(false);
        BtnLogOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnLogOutActionPerformed(evt);
            }
        });
        BodyLayout.add(BtnLogOut, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 80, 30));

        BgLayout.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Bglbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 0, 100, 100));

        Bglbl1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 100, 100));

        Bglbl2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 0, 100, 100));

        Bglbl3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl3, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 0, 100, 100));

        Bglbl4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl4, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 0, 100, 100));

        Bglbl5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl5, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 0, 100, 100));

        Bglbl6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl6, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 0, 100, 100));

        Bglbl7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl7, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 0, 100, 100));

        Bglbl8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl8, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 100, 100, 100));

        Bglbl9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 100, 100));

        Bglbl10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl10, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 100, 100, 100));

        Bglbl11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl11, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 100, 100, 100));

        Bglbl12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl12, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 100, 100, 100));

        Bglbl13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl13, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 100, 100, 100));

        Bglbl14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl14, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 100, 100, 100));

        Bglbl15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl15, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 100, 100, 100));

        Bglbl16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl16, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 200, 100, 100));

        Bglbl17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl17, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 200, 100, 100));

        Bglbl18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl18, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 200, 100, 100));

        Bglbl19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl19, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 200, 100, 100));

        Bglbl20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl20, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 200, 100, 100));

        Bglbl21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl21, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 200, 100, 100));

        Bglbl22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl22, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 200, 100, 100));

        Bglbl23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl23, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 200, 100, 100));

        Bglbl24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl24, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 300, 100, 100));

        Bglbl25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl25, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 300, 100, 100));

        Bglbl26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl26, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 300, 100, 100));

        Bglbl27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl27, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 300, 100, 100));

        Bglbl28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl28, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 300, 100, 100));

        Bglbl29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl29, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 300, 100, 100));

        Bglbl30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl30, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 300, 100, 100));

        Bglbl31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Source/ingredients_background_sm.jpg"))); // NOI18N
        BgLayout.add(Bglbl31, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 300, 100, 100));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 800, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(BodyLayout, javax.swing.GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(BgLayout, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(BodyLayout, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(BgLayout, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnLogOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnLogOutActionPerformed
        Kangaroo.goTo(LoginController.class);
    }//GEN-LAST:event_BtnLogOutActionPerformed

    private void CbxClientsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CbxClientsActionPerformed
        if (loading) return;
        refreshTable();
    }//GEN-LAST:event_CbxClientsActionPerformed

    void refreshTable() {
        var clientId = CbxClients.getSelectedItem();
        var client = clients.get(clientId.toString());
        orders = controller.getClientOrders(client);
        var model = (DefaultTableModel) Table.getModel();
        model.setRowCount(0);
        orders.forEach((order) -> {
            model.addRow(new Object[]{
                client.name(),
                client.gender(),
                order.province(),
                order.id(),
            });
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLayeredPane BgLayout;
    private javax.swing.JLabel Bglbl;
    private javax.swing.JLabel Bglbl1;
    private javax.swing.JLabel Bglbl10;
    private javax.swing.JLabel Bglbl11;
    private javax.swing.JLabel Bglbl12;
    private javax.swing.JLabel Bglbl13;
    private javax.swing.JLabel Bglbl14;
    private javax.swing.JLabel Bglbl15;
    private javax.swing.JLabel Bglbl16;
    private javax.swing.JLabel Bglbl17;
    private javax.swing.JLabel Bglbl18;
    private javax.swing.JLabel Bglbl19;
    private javax.swing.JLabel Bglbl2;
    private javax.swing.JLabel Bglbl20;
    private javax.swing.JLabel Bglbl21;
    private javax.swing.JLabel Bglbl22;
    private javax.swing.JLabel Bglbl23;
    private javax.swing.JLabel Bglbl24;
    private javax.swing.JLabel Bglbl25;
    private javax.swing.JLabel Bglbl26;
    private javax.swing.JLabel Bglbl27;
    private javax.swing.JLabel Bglbl28;
    private javax.swing.JLabel Bglbl29;
    private javax.swing.JLabel Bglbl3;
    private javax.swing.JLabel Bglbl30;
    private javax.swing.JLabel Bglbl31;
    private javax.swing.JLabel Bglbl4;
    private javax.swing.JLabel Bglbl5;
    private javax.swing.JLabel Bglbl6;
    private javax.swing.JLabel Bglbl7;
    private javax.swing.JLabel Bglbl8;
    private javax.swing.JLabel Bglbl9;
    private javax.swing.JLayeredPane BodyLayout;
    private javax.swing.JButton BtnLogOut;
    private javax.swing.JComboBox<String> CbxClients;
    private javax.swing.JTable Table;
    private javax.swing.JTextArea TxtOrderData;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables
}
