/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Controllers.Auth;

import App.Frames.Auth.RegisterFrame;
import App.Models.Client;
import FBMY.navigation.Controller;
import FBMY.navigation.Kangaroo;
import FBMY.types.Request;
import java.util.UUID;
import javax.swing.JOptionPane;

/**
 *
 * @author Yor
 */
public class RegisterController extends Controller<RegisterFrame> {
    public RegisterController(RegisterFrame frame) { super(frame); }
    
    @Override
    public void index() {
        frame.setVisible(true);
    }
    
    public void register(Request request) {
        try {
            var identification = (String) request.get("identification");
            if (identification == null || identification.isBlank()) 
                throw new Exception("No id card found");
            
            var clients = Client.ctx.where("identification", identification);
            if (!clients.get().isEmpty())
                throw new Exception("Id card already in use");
            
            var name = (String) request.get("name");
            if (name == null || name.isBlank()) 
                throw new Exception("No name found");
            
            var province = (String) request.get("province");
            if (province == null || province.isBlank()) 
                throw new Exception("Please select a gender");
            
            var gender = (String) request.get("gender");
            if (gender == null || gender.isBlank()) 
                throw new Exception("Please select a gender");
            
            Client.ctx.create((builder) -> {
                request.forEach(builder::set);
                builder.set("id", UUID.randomUUID());
                builder.set("role", Client.Role.user.toString());
            });
            Kangaroo.goTo(LoginController.class);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
}
