/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Controllers.Auth;

import App.Controllers.OrderController;
import App.Controllers.ReportsController;
import App.Frames.Auth.LoginFrame;
import App.Models.Client;
import App.State;
import FBMY.navigation.Controller;
import FBMY.navigation.Kangaroo;
import javax.swing.JOptionPane;

/**
 *
 * @author Yor
 */
public class LoginController extends Controller<LoginFrame> {
    public LoginController(LoginFrame frame) { super(frame); }
    final String NO_ROLE = "User role is not in our registers"; 
    
    @Override
    public void index() {
        State.Auth = null;
        frame.setVisible(true);
    }
    
    public void login(String identification) {
        try {
            if (identification == null || identification.isBlank()) 
                throw new Exception("Please enter an identication");
            var client = Client.ctx.where("identification", identification).get();
            if (client.isEmpty())
                throw new Exception("No User found");
            State.Auth = client.get(0);
            switch (State.Auth.role()) {
                case admin -> Kangaroo.goTo(ReportsController.class);
                case user -> Kangaroo.goTo(OrderController.class);
                default -> throw new Exception(NO_ROLE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
}
