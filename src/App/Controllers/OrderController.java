/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Controllers;

import App.Controllers.Auth.LoginController;
import App.Frames.OrderFrame;
import App.Models.Order;
import App.State;
import FBMY.navigation.Controller;
import FBMY.navigation.Kangaroo;
import FBMY.types.DateTime;
import FBMY.types.Request;
import java.util.UUID;
import javax.swing.JOptionPane;

/**
 *
 * @author Yor
 */
public class OrderController extends Controller<OrderFrame> {
    public OrderController(OrderFrame frame) { super(frame); }

    @Override
    public void index() {
        frame.setVisible(true);
    }
    
    public void makeOrder(Request request) {
        try {
            if (State.Auth == null) 
                throw new Exception("No able to access the current session, please logout and try again");
            Order.ctx.create((arg) -> {
                request.forEach(arg::set);
                arg.set("id", UUID.randomUUID());
                arg.set("client_id", State.Auth.id());
                arg.set("province", State.Auth.province());
                arg.set("created_at", new DateTime());
                if (((String) request.get("discount_code")).isBlank()) {
                    arg.set("discount_code", null);
                }
            });
            JOptionPane.showMessageDialog(null, "Successfully Ordered");
            Kangaroo.goTo(LoginController.class);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No able to access the current session, please logout and try again");
        }
    }
    
}
