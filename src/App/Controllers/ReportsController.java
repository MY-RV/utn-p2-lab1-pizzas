/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Controllers;

import App.Frames.ReportsFrame;
import App.Models.Client;
import App.Models.Order;
import FBMY.navigation.Controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Yor
 */
public class ReportsController extends Controller<ReportsFrame> {
    public ReportsController(ReportsFrame frame) { super(frame); }

    @Override
    public void index() {
        frame.setVisible(true);
    }
    
    public Map<String, Client> getClientsInMap() {
        var clients = Client.ctx.where("role", Client.Role.user).get();
        var response = new HashMap();
        clients.forEach((client) -> {
           response.put(client.identification(), client);
        });
        return response;
    }
    
    public List<Order> getClientOrders(Client client) {
        return Order.ctx.where("client_id", client.id()).get();
    }
    
}
