/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App;

import App.Models.Order;
import FBMY.Config;
import FBMY.injections.ModelsBuilder;
import FBMY.treats.dataWorkers.Parrot;
import FBMY.types.SimpleJSON;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Yor
 */
public class Bootapp {
    public void setUp() {
        // Set the csv files directory for models
        Config.DataDir(Config.APPDIR + "/src/Datasets/");
        
        Parrot.learn(SimpleJSON.class, (value) -> 
            new SimpleJSON('\'', String.valueOf(value))
        );
        Parrot.learn(Order.AddonsList.class, (value) -> {
            var stream = List.of(String.valueOf(value).split(",")).stream();
            var response = new Order.AddonsList();
            stream.map(String::valueOf).map((t) -> 
                Parrot.Translate(Order.Addon.class, t.trim())
            ).forEach(response::add);
            return response;
        });
        Parrot.learn(Character.class, (value) -> 
            String.valueOf(value).charAt(0)
        );
        
        // Inject all the ctx for Models Classes
        ModelsBuilder.UseDefault();
        ModelsBuilder.run();
    }
}
